from bs4 import BeautifulSoup
import re
from urllib.request import urlopen
import time
from sms import *

def replace(obj):
  if obj.group(0) == '-':
    return ''
  else:
    return '-'

def parse_html(tweet_store):
  # get html
  url = 'https://twitter.com/ChipotleTweets'
  content = urlopen(url)

  # parse html
  soup = BeautifulSoup(content, 'html.parser')

  # find tweets
  results = soup.find_all("p", {"class":"tweet-text"})

  # reset counter for second message (pinned message at the top)
  # set to
  counter = 0

  # clean up results
  tags = '<.*>'
  for r in results:
    cleaned = re.sub('<.*>', '', r.text)
    cleaned = re.sub(r'[^\x00-\x7F]+', '', cleaned)
    cleaned = re.sub('\n', ' ', cleaned)

    #find #ChipotleFreeting
    if cleaned.find('#ChipotleFreeting') > -1:
      #if latest message after pinned message is different than before
      if (counter == 0) and (tweet_store != cleaned): #counter = 0 checks which tweet specifically (1st, 2nd, etc)
          # # code is in format 'FREE' + 5 characters A-Z and 0-9
          # code_search_1 = re.search(r'\sFREE[A-Z0-9]{5}\s', cleaned) #search for code with whitespace on either end
          # code_search_2 = re.search(r'\s[A-Z0-9]{5}FREE\s', cleaned)
          # code_search_3 = re.search(r'\s[A-Z0-9]{2}FREE[A-Z0-9]{3}\s', cleaned)

          search_type = [r'\sFREE[A-Z0-9]{5}\s',
                        r'\s[A-Z0-9]{5}FREE\s',
                        r'\s[A-Z0-9]{2}FREE[A-Z0-9]{3}\s',
                        r'\s[A-Z0-9]{3}FREE[A-Z0-9]{2}\s',
                        r'\s[A-Z0-9]{1}FREE[A-Z0-9]{4}\s',
                        r'\s[A-Z0-9]{4}FREE[A-Z0-9]{1}\s']
          # if (code_search_1):
          #   code = code_search_1.group(0) # get actual code
          #   code = code[1:-1] #remove whitespace
          #   print("Code: " + str(code))
          #   n = 1 #return 1 as code
          # elif (code_search_2):
          #   code = code_search_2.group(0) # get actual code
          #   code = code[1:-1] #remove whitespace
          #   print("Code: " + str(code))
          #   n = 1 #return 1 as code
          # elif (code_search_3):
          #   code = code_search_3.group(0) # get actual code
          #   code = code[1:-1] #remove whitespace
          #   print("Code: " + str(code))
          #   n = 1 #return 1 as code
          # else:
          #   n = 0 #return 0 as code
          #   code = "" #return empty code

          for ii in range(len(search_type)): #loops through the various code patterns
            code_search = re.search(search_type[ii], cleaned)

            #it pattern found
            if (code_search):
              code = code_search.group(0) # get actual code
              code = code[1:-1] #remove whitespace
              print("Code: " + str(code))
              n = 1 #return n = 1 if new code in tweet!
              break #break for loop
            else:
                n = 0 #return 0 if no code found in new tweet
                code = "" #return emtpy code
          tweet_store = cleaned #save new message
          return n, tweet_store, code
      else:
          #else add one to counter
          counter += 1

  n = 0 #return 0 if nothing new
  code = ""
  return n, tweet_store, code

if __name__ == "__main__":
  tweet_store = parse_html("")[1]
  # driver = gv_login()
  driver = m_login()
  print("Program Initialized\nInitial Tweet Store:")
  print(tweet_store)

  i = 0
  num_store = 0
  while True:
      n, tweet_store, code = parse_html(tweet_store)

      if n == 1: #if code found in new tweet
          # gv_sms(driver, code)
          num_store = m_sms(driver, code, 888222, num_store)
          print("Code Sent")

      time.sleep(0.5)
      i += 1
      print(i)
