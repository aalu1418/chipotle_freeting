# Chipotle Freeting - 2019
Because https://www.chipotle.com/freedelivery.html#freeting
Free chipotle everytime a 2019 NBA Finals announcer says "free" (20 times)
500 burritos every time in the first half, 1000 burritos every time in the second half

### Final Implementation
Invovled a web scraping, regex, and selenium web driver.
Web scraping to pull data from Twitter, regex to parse the code, and selenium to send using the android messages web interface.

#### Twitter API
~~Authentication~~
* ~~https://developer.twitter.com/en/docs/basics/authentication/overview/authentication-and-authorization.html~~

~~Filter realtime tweets~~
* ~~https://developer.twitter.com/en/docs/tweets/filter-realtime/overview~~
* ~~https://developer.twitter.com/en/docs/tweets/rules-and-filtering/overview/standard-operators.html~~

~~Rate Limits~~
* ~~https://developer.twitter.com/en/docs/basics/rate-limits~~

#### beautifulsoup4 - scrape website HTML/XML
* https://pypi.org/project/beautifulsoup4/

#### App Password for Google
* https://support.google.com/accounts/answer/185833?hl=en

#### PyGoogleVoice
* ~~http://sphinxdoc.github.io/pygooglevoice/~~ (google api no longer works)

#### TextMagic
* ~~https://www.textmagic.com/ (free 30 day trial)~~ not supported in area :(

#### Interacting Directly with Webpage
* https://selenium-python.readthedocs.io/
