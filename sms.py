from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import ActionChains

import getpass
import time

def gv_login():
    usrnm = input("Username: ")
    psswrd = getpass.getpass(prompt = 'Password: ')

    #navigating to webpage
    driver = webdriver.Firefox()
    driver.get("https://voice.google.com/signup")

    #inputting username
    login = driver.find_element_by_id('identifierId')
    login.send_keys(usrnm)
    login.send_keys(Keys.RETURN)

    #inputting password
    password = WebDriverWait(driver,10).until(
                EC.presence_of_element_located((By.NAME, "password")))
    password.send_keys(psswrd)
    time.sleep(1)
    password.send_keys(Keys.RETURN)

    #nagivating to messages
    time.sleep(1)
    driver.get("https://voice.google.com/u/0/messages")
    time.sleep(1)

    return driver

def gv_sms(driver, text_content, num=888222):
    #begin new message
    new_message = driver.find_element_by_xpath("//div[@gv-id = 'send-new-message']")
    actions = ActionChains(driver)
    actions.move_to_element(new_message)
    actions.click()
    actions.perform()
    # time.sleep(1)

    #input phone number
    phone_num = driver.find_element_by_xpath("//input[@id = 'input_0']")
    # time.sleep(1)
    phone_num.send_keys(num)
    phone_num.send_keys(Keys.RETURN)

    #input message
    text = driver.find_element_by_xpath("//textarea[@id = 'input_1']")
    # time.sleep(1)
    text.send_keys(text_content)
    text.send_keys(Keys.RETURN)
    # time.sleep(1)
    driver.refresh() #refresh the page to keep input_0 as input_0
    return 0

def m_login():
    driver = webdriver.Firefox()
    driver.get("https://messages.google.com/web/authentication?redirected=true")
    print('Please Login on Firefox')

    return driver

def m_sms(driver, text_content, num=888222, num_store = 0):

    #if num store = num (assume message is already pulled up)
    if num_store != num:
        #begin new message
        new_message = driver.find_element_by_xpath("//a[@class = 'new-chat mat-button']")
        actions = ActionChains(driver)
        actions.move_to_element(new_message)
        actions.click()
        actions.perform()
        time.sleep(1)

        #input phone number
        phone_num = driver.find_element_by_xpath("//input[@placeholder = 'Type a name, phone number, or email']")
        phone_num.send_keys(num)
        phone_num.send_keys(Keys.RETURN)
        time.sleep(3)

    #input message
    text = driver.find_element_by_xpath("//textarea[@placeholder = 'Text message']")
    text.send_keys(text_content)
    text.send_keys(Keys.RETURN)

    num_store = num

    return num_store

if __name__ == "__main__":
    #testing google voice integration
    # driver = gv_login()
    # gv_sms(driver, "this is a test", 2488028249)

    #testing android messages integration
    driver = m_login()
    time.sleep(10)
    num = 0
    for i in range(3):
        start = time.time()
        text = "test: "+ str(i)
        num = m_sms(driver,text, 7342155857, num) #stores number used
        # num = m_sms(driver,text, 7342155857) #does not store number
    end = time.time()
    print(end-start)
